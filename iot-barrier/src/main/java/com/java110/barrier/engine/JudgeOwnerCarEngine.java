package com.java110.barrier.engine;

import com.java110.core.service.car.ICarInoutService;
import com.java110.core.service.car.ICarService;
import com.java110.core.util.DateUtil;
import com.java110.core.util.StringUtil;
import com.java110.entity.car.CarDayDto;
import com.java110.entity.car.CarDto;
import com.java110.entity.car.CarInoutDto;
import com.java110.entity.machine.MachineDto;
import com.java110.entity.parkingArea.ParkingAreaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JudgeOwnerCarEngine {


    @Autowired
    private ICarService carServiceImpl;

    @Autowired
    private ICarInoutService carInoutServiceImpl;


    public CarDayDto judgeOwnerCar(MachineDto machineDto, String carNum, List<ParkingAreaDto> parkingAreaDtos) throws Exception {
        List<String> paIds = new ArrayList<>();
        for (ParkingAreaDto parkingAreaDto : parkingAreaDtos) {
            paIds.add(parkingAreaDto.getPaId());
        }
        CarDto carDto = new CarDto();
        carDto.setPaIds(paIds.toArray(new String[paIds.size()]));
        carDto.setCarNum(carNum);
        List<CarDto> carDtos = carServiceImpl.queryCars(carDto);

        if (carDtos == null || carDtos.size() < 1) {
            return new CarDayDto(carNum, CarDto.LEASE_TYPE_TEMP, -1);
        }

        //主副车辆中 有一个车辆在场，这个车场当做临时车处理
        if (hasInParkingArea(carDtos.get(0).getPrimaryCarId(),carNum, paIds)) {
            return new CarDayDto(carNum, CarDto.LEASE_TYPE_TEMP, -1);
        }


        int day = DateUtil.differentDaysUp(DateUtil.getCurrentDate(), carDtos.get(0).getEndTime());
        //来个五天 缴费期
        if (day <= 0 && day >-5) {
            return new CarDayDto(carNum, carDtos.get(0).getLeaseType(), -2);
        }

        if(day <= -5){
            return new CarDayDto(carNum, CarDto.LEASE_TYPE_TEMP, -1);
        }
        return new CarDayDto(carNum, carDtos.get(0).getLeaseType(), day);
    }

    /**
     * 判断 主副车辆中 有一个车辆在场，这个车场当做临时车处理
     *
     * @param carNum
     * @return
     */
    private boolean hasInParkingArea(String primaryCarId, String carNum, List<String> paIds) throws Exception {
        if (StringUtil.isEmpty(primaryCarId) || "-1".equals(primaryCarId)) {
            return false;
        }

        // 判断是否为主副车辆
        CarDto carDto = new CarDto();
        carDto.setPaIds(paIds.toArray(new String[paIds.size()]));
        carDto.setCarNum(carNum);
        carDto.setPrimaryCarId(primaryCarId);
        List<CarDto> tmpCarDtos = carServiceImpl.queryCars(carDto);
        if (tmpCarDtos == null || tmpCarDtos.size() < 2) {
            return false;
        }

        List<String> otherCarNums = new ArrayList<>();

        for (CarDto tempCarDto : tmpCarDtos) {
            if (tempCarDto.getCarNum().equals(carNum)) {
                continue;
            }
            otherCarNums.add(tempCarDto.getCarNum());
        }

        if (otherCarNums.size() < 1) {
            return false;
        }

        for (String otherCarNum : otherCarNums) {
            //判断车辆是否在 场内
            CarInoutDto inoutDto = new CarInoutDto();
            inoutDto.setCarNum(otherCarNum);
            inoutDto.setPaIds(paIds.toArray(new String[paIds.size()]));
            inoutDto.setState("1");
            List<CarInoutDto> carInoutDtos = carInoutServiceImpl.queryCarInout(inoutDto);
            if (carInoutDtos != null && carInoutDtos.size() > 0) {
                return true;
            }
        }

        return false;
    }
}
