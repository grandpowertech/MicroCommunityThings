package com.java110.core.service.car;

import com.java110.entity.car.CarInoutDto;
import com.java110.entity.car.CarInoutTempAuthDto;
import com.java110.entity.machine.MachineDto;
import com.java110.entity.response.ResultDto;

import java.util.List;

/**
 * @ClassName ICarInoutService
 * @Description TODO 临时车进场审核
 * @Author wuxw
 * @Date 2020/5/14 14:48
 * @Version 1.0
 * add by wuxw 2020/5/14
 **/
public interface ICarInoutTempAuthService {

    /**
     * 保存临时车审核
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     * @throws Exception
     */
    int saveCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto, MachineDto machineDto);

    /**
     * 查询临时车审核 总数
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     * @throws Exception
     */
    long queryCarInoutTempAuthCount(CarInoutTempAuthDto carInoutTempAuthDto);

    /**
     * 查询临时车审核
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     * @throws Exception
     */
    List<CarInoutTempAuthDto> queryCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) ;

    /**
     * 修改临时车审核
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     * @throws Exception
     */
    int updateCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) ;

    /**
     * 删除临时车审核
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     * @throws Exception
     */
    int deleteCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) ;

}
